import { call, put, all, takeEvery } from 'redux-saga/effects';
import { Post } from '../../../models/post';
import { getPosts } from '../../../api/jsonPlaceholder';
import { fetchPosts } from '../../actions';

function* handleFetch() {
    try {
        const response: Post[] = yield call(getPosts);
        yield put(fetchPosts.success(response));
    } catch (error) {
        yield put(fetchPosts.failure(error));
    }
}

export function* postsSaga() {
    yield all([takeEvery(fetchPosts.request, handleFetch)]);
}