import { call, put, all, takeEvery } from 'redux-saga/effects';
import { User } from '../../../models/user';
import { getUser } from '../../../api/jsonPlaceholder';
import { fetchUser } from '../../actions';

function* handleFetch(action: ReturnType<typeof fetchUser.request>) {
    try {
        console.log(action);
        const response: User = yield call(getUser, action.payload);
        yield put(fetchUser.success(response));
    } catch (error) {
        yield put(fetchUser.failure(error));
    }
}

export function* userSaga() {
    yield all([takeEvery(fetchUser.request, handleFetch)]);
}