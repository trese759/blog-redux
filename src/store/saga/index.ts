import { all, fork } from 'redux-saga/effects';
import { postsSaga } from './post/fetchPosts';
import { userSaga } from './user/fetchUser';

export function* rootSaga() {
    yield all([
        fork(postsSaga),
        fork(userSaga)
    ]);
}