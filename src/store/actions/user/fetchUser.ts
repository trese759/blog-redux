import { createAsyncAction } from 'typesafe-actions';
import { FetchUser } from '../../types/user/fetchUser';
import { User } from '../../../models/user';

export const fetchUser = createAsyncAction(
    FetchUser.FETCH_USER_REQUEST,
    FetchUser.FETCH_USER_SUCCESS,
    FetchUser.FETCH_USER_ERROR
)<number, User, string>();

/*
const fetchUserRequest = (id: number) => 
    action(FetchUser.FETCH_USER_REQUEST, undefined, { id });

export const fetchUserSuccess = (user: User) => 
    action(FetchUser.FETCH_USER_SUCCESS, user);

export const fetchUserError = (error: string) => 
    action(FetchUser.FETCH_USER_ERROR, error);

export const fetchUser = (id: number) => 
    action(FetchUser.FETCH_USER, fetchUserRequest(id));
*/