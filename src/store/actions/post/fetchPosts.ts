import { createAsyncAction } from 'typesafe-actions';
import { FetchPosts } from '../../types/post/fetchPost';
import { Post } from '../../../models/post';

export const fetchPosts = createAsyncAction(
    FetchPosts.FETCH_POSTS_REQUEST,
    FetchPosts.FETCH_POSTS_SUCCESS,
    FetchPosts.FETCH_POSTS_ERROR
)<void, Post[], string>();

/*
export const fetchPostsSuccess = (post: Post[]) => action(
    FetchPosts.FETCH_POSTS_SUCCESS, post
);

export const fetchPostsError = (error: string) => action(
    FetchPosts.FETCH_POSTS_ERROR, error
);

export const fetchPosts = () => action(
    FetchPosts.FETCH_POSTS, fetchPostsRequest
);
*/