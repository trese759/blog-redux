import { createSelector } from "reselect";
import { User } from "../../../models/user";
import { ApplicationState } from "../../states/application";
import { useDispatch, useSelector } from "react-redux";
import { useCallback, useEffect } from "react";
import { fetchUser } from "../../actions";

const getUsers = (state: ApplicationState) => state.users.data;
const getIsFetching = (state: ApplicationState) => state.users.baseState.isFetching;
const getIsFetched = (state: ApplicationState) => state.users.baseState.isFetched;

const selectorGetUsers = createSelector(
    [getUsers], (users: User[]) => users
);

const selectorIsFetching = createSelector(
    [getIsFetching], (fetching) => fetching
);

const selectorIsFetched = createSelector(
    [getIsFetched], (fetched) => fetched
);

export const useGetUserById = (id: number) => {
    const dispatch = useDispatch();

    const users = useSelector<ApplicationState, User[]>(selectorGetUsers);
    const isFetched = useSelector<ApplicationState, boolean>(selectorIsFetched);
    const isFetching = useSelector<ApplicationState, boolean>(selectorIsFetching);

    const userFound = users.find(user => user.id === id);
    const userFoundExist = !!userFound;

    const fetchUserById = useCallback((id: number) => {
        dispatch(fetchUser.request(id));
    }, [dispatch]);

    useEffect(() => {
        if (!isFetched && !isFetching && !userFoundExist) {
            fetchUserById(id);
        }
    }, [isFetched, isFetching, fetchUserById, id, userFoundExist]);

    return {
        user: userFound,
        isFetched
    }
};