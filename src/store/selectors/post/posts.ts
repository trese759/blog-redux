import { createSelector } from 'reselect';
import { useDispatch, useSelector } from 'react-redux';
import { fetchPosts, fetchUser } from '../../actions';
import { useEffect, useCallback } from 'react';
import { Post } from '../../../models/post';
import { ApplicationState } from '../../states/application';
import { User } from '../../../models/user';

const getPosts = (state: ApplicationState) => state.posts.data;
const getUsers = (state: ApplicationState) => state.users.data;
const getIsFetching = (state: ApplicationState) => state.posts.baseState.isFetching;
const getIsFetched = (state: ApplicationState) => state.posts.baseState.isFetched;

const selectorPosts = createSelector(
    [getPosts], (posts) => posts
);

const selectorGetUsers = createSelector(
    [getUsers], (users: User[]) => users
);

const selectorIsFetching = createSelector(
    [getIsFetching], (fetching) => fetching
);

const selectorIsFetched = createSelector(
    [getIsFetched], (fetched) => fetched
);

export const usePosts = () => {
    const dispatch = useDispatch();

    const posts = useSelector<ApplicationState, Post[]>(selectorPosts);
    const isFetched = useSelector<ApplicationState, boolean>(selectorIsFetched);
    const isFetching = useSelector<ApplicationState, boolean>(selectorIsFetching);

    const fetchAllPosts = useCallback((): void => {
        dispatch(fetchPosts.request());
    }, [dispatch]);

    useEffect(() => {
        if (!isFetched && !isFetching) {
            fetchAllPosts();
            console.log('fetched')
        }
    }, [isFetched, isFetching, fetchAllPosts]);

    return {
        posts,
        isFetched,
        isFetching,
        fetchAllPosts
    }
};

export const useLoadAuthors = () => {
    const dispatch = useDispatch();

    const posts = useSelector<ApplicationState, Post[]>(selectorPosts);
    const isFetched = useSelector<ApplicationState, boolean>(selectorIsFetched);
    const users = useSelector<ApplicationState, User[]>(selectorGetUsers);
    
    const fetchSingleUser = useCallback((id: number = 0) => {
        dispatch(fetchUser.request(id));
    }, [dispatch]);

    const fetchDistinctUsers = useCallback(() => {
        const uniqueAuthorPosts = Array.from(new Set(posts.map(post => post.userId)))
            .map(id => posts.find(post => post.userId === id));

        console.log(uniqueAuthorPosts);
        uniqueAuthorPosts.forEach(post => fetchSingleUser(post?.userId));
    }, [fetchSingleUser, posts]);
    
    useEffect(() => {
        if (isFetched && users.length === 0) {
            fetchDistinctUsers();
        }
    }, [fetchDistinctUsers, isFetched, users]);

    return {
        users
    };
}