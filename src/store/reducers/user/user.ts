import { UserState } from "../../states/user/user";
import { User } from "../../../models/user";
import { Reducer } from "redux";
import { createReducer } from "typesafe-actions";
import { FetchUser } from "../../types/user/fetchUser";

const initialState: UserState = {
    data: [],
    baseState: {
        error: '',
        isFetched: false,
        isFetching: true,
        rollback: []
    }
};

const userReducer: Reducer<UserState> = createReducer(initialState)
    .handleAction(FetchUser.FETCH_USER_REQUEST, (state: UserState): UserState  => state)
    .handleAction(FetchUser.FETCH_USER_SUCCESS, (state: UserState, action: any): UserState => {
        return {
            ...state,
            data: [ ...state.data, action.payload as User],
            baseState: {
                error: '',
                isFetched: true,
                isFetching: false,
                rollback: state.data
            }
        };
    })
    .handleAction(FetchUser.FETCH_USER_ERROR, (state: UserState, action: any): UserState => {
        return {
            ...state,
            data: state.data,
            baseState: {
                error: action.payload,
                isFetched: true,
                isFetching: false,
                rollback: state.data
            }
        };
    });

export default userReducer;