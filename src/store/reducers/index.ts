import { combineReducers } from "redux";
import { postsReducer } from './post';
import { userReducer } from './user';

const rootReducer = combineReducers({
    posts: postsReducer,
    users: userReducer
});

export default rootReducer;