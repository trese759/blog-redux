import { createReducer } from "typesafe-actions";
import { FetchPosts } from "../../types/post/fetchPost";
import { PostState } from "../../states/post/post";
import { Post } from "../../../models/post";
import { Reducer } from "redux";

const initialState: PostState = {
    data: [],
    baseState: {
        error: '',
        isFetched: false,
        isFetching: true,
        rollback: []
    }
};

const postsReducer: Reducer<PostState> = createReducer<PostState>(initialState)
    .handleAction(FetchPosts.FETCH_POSTS_REQUEST, (state: PostState): PostState => state)
    .handleAction(FetchPosts.FETCH_POSTS_SUCCESS, (state: PostState, action: any): PostState => {
        return { 
            ...state, 
            data: action.payload as Post[],
            baseState: {
                isFetching: false,
                isFetched: true,
                rollback: state.data,
                error: ''
            }
        };
    })
    .handleAction(FetchPosts.FETCH_POSTS_ERROR, (state: PostState, action: any): PostState => {
        return { 
            ...state,
            data: [],
            baseState: {
                isFetching: false,
                isFetched: true,
                error: action.payload,
                rollback: state.data
            }
        };
    });

export default postsReducer;