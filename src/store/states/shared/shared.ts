export interface SharedState {
    readonly isFetching: boolean;
    readonly isFetched: boolean;
    readonly rollback: any;
    readonly error: string;
}