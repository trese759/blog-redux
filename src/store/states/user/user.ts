import { User } from "../../../models/user";
import { SharedState } from "../shared/shared";

export interface UserState {
    readonly data: User[];
    readonly baseState: SharedState;
}