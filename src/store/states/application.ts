import { PostState } from "./post/post";
import { UserState } from "./user/user";

export interface ApplicationState {
    posts: PostState;
    users: UserState;
}