import { Post } from "../../../models/post";
import { SharedState } from "../shared/shared";

export interface PostState {
    readonly data: Post[];
    readonly baseState: SharedState;
}