import createSagaMiddleware from 'redux-saga';
import { Store, createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from 'redux-devtools-extension';

import { rootSaga } from './saga';
import reducers from './reducers';
import { ApplicationState } from "./states/application";

const storeConfig = (initialState: ApplicationState): Store<ApplicationState> => {
    const sagaMiddleware = createSagaMiddleware();
    const store = createStore(
        reducers, 
        initialState,
        composeWithDevTools(applyMiddleware(sagaMiddleware))
    );

    sagaMiddleware.run(rootSaga);
    return store;
}

export default storeConfig;