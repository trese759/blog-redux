import React from 'react';
import ReactDOM from 'react-dom';

import App from './components/App';
import { ApplicationState } from './store/states/application';
import { Provider } from 'react-redux';
import storeConfig from './store/storeConfig';
import { SharedState } from './store/states/shared/shared';

const initialBaseState: SharedState = {
    error: '',
    isFetched: false,
    isFetching: false,
    rollback: []
};

const initialState: ApplicationState = {
    posts: {
        data: [],
        baseState: initialBaseState
    },
    users: {
        data: [],
        baseState: initialBaseState
    }
};

ReactDOM.render(
    <Provider store={storeConfig(initialState)}>
        <App />
    </Provider>,
    document.querySelector('#root')
);