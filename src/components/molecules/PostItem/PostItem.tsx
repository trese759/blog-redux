import React, { memo } from 'react';

import { Post } from '../../../models/post';
import { PostImage, PostTitle, PostAuthor, PostDescription } from '../../atoms';
import { User } from '../../../models/user';

export interface Props {
    post: Post;
    user?: User;
};

const PostItem: React.FC<Props> = ({ post, user }): JSX.Element => {
    return (
        <div className="item">
            <PostImage imageName="user" />
            <div className="content">
                <PostTitle title={post.title} />
                <PostAuthor author={user} />
                <PostDescription description={post.body} />
            </div>
        </div>
    );
};

export default memo(PostItem);