import React, { memo } from 'react';

export interface Props {
    description: string;
};

const PostDescription: React.FC<Props> = ({ description }): JSX.Element => {
    return (
        <div className="description">
            <p>{ description }</p>
        </div>
    );
};

export default memo(PostDescription);