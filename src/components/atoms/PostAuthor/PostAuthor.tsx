import React, { memo } from 'react';
import { User } from '../../../models/user';

export interface Props {
    author?: User;
}

const PostAuthor: React.FC<Props> = ({author}): JSX.Element => {
    return (
        <div className="meta">
            <span>{author?.name}</span>
        </div>
    );
};

export default memo(PostAuthor);