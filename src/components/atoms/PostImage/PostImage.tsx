import React, { memo } from 'react';

export interface Props {
    imageName: string;
};

const PostImage: React.FC<Props> = ({ imageName }): JSX.Element => {
    return (
        <i 
            className={`huge middle aligned icon ${imageName}`}
            style={{marginRight: 15}}
        />
    );
};

export default memo(PostImage);