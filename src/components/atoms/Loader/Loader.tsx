import React, { memo } from 'react';

export enum Size {
    small = 'small',
    medium = 'medium',
    large = 'large'
};

export interface Props {
    size: Size;
    isCentered: boolean;
}

const Loader: React.FC<Props> = ({size, isCentered}): JSX.Element => {
    return <div className={`ui active ${isCentered ? 'centered' : ''} ${size} inline loader`} />;
};

export default memo(Loader);