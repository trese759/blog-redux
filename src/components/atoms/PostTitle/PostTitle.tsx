import React, { memo } from 'react';

export interface Props {
    title: string;
};

const PostTitle: React.FC<Props> = ({ title }): JSX.Element => {
    return <div className="header">{ title }</div>;
};

export default memo(PostTitle);