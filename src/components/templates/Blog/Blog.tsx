import React, { memo } from 'react';
import { PostList } from '../../organisms';

const Blog: React.FC = (): JSX.Element => {
    return (
        <div className="ui container" style={{marginTop: 15}}>
            <h1>Blog Posts</h1>
            <hr />
            <PostList />
        </div>
    );
};

export default memo(Blog);