import React, { memo } from 'react';
import { PostItem } from '../../molecules';
import { usePosts, useLoadAuthors } from '../../../store/selectors/post/posts';
import { Loader } from '../../atoms';
import { Size } from '../../atoms/Loader/Loader';

const PostList: React.FC = (): JSX.Element => {
    const {posts, isFetched} = usePosts();
    const { users } = useLoadAuthors();

    const renderLoader = () => <Loader isCentered={true} size={Size.large} />;
    const renderPosts = () => {
        return (
            <div className="ui items">
                {posts.map(post => {
                    const userFound = users.find(user => user.id === post.userId);
                    return <PostItem key={post.id} post={post} user={userFound} />;
                })}
            </div>
        );
    }

    if (isFetched) {
        return renderPosts();
    } else {
        return renderLoader();
    }
};

export default memo(PostList);