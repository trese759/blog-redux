import React, { memo } from 'react';

import { Blog } from './templates';

const App: React.FC = (): JSX.Element => {
    return <Blog />;
};

export default memo(App);