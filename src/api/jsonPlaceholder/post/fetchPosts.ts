import wretch, { ResponseChain } from 'wretch';

import { Post } from '../../../models/post';

export const getPosts = async (): Promise<Post[]> => {
    const response: ResponseChain = await wretch()
        .url('https://jsonplaceholder.typicode.com/posts').get();

    return response.json();
}