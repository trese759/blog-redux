import wretch, { ResponseChain } from 'wretch';
import { User } from '../../../models/user';

export const getUser = async (id: number): Promise<User> => {
    const response: ResponseChain = await wretch()
        .url(`https://jsonplaceholder.typicode.com/users/${id}`).get();

    return response.json();
}